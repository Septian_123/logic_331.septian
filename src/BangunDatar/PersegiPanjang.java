package BangunDatar;

import java.util.Scanner;

public class PersegiPanjang {

    private static Scanner input = new Scanner(System.in);

    private static int panjang;
    private static int lebar;

    public static void Luas ()
    {
//        Luas Persegi Panjang
        System.out.println("Perhitungan Luas Persegi Panjang : ");
        System.out.println("Input Panjang (cm)");
        panjang = input.nextInt();
        System.out.println("Input Lebar (cm)");
        lebar = input.nextInt();

        int luaspersegiP = panjang * lebar;
        System.out.println("Luas Persegi Panjang adalah = " + luaspersegiP);
    }
    public static void Keliling ()
    {
        //  Keliling Persegi Panjang
        System.out.println("Perhitungan Keliling Persegi Panjang : ");
        System.out.println("Input Panjang (cm)");
        panjang = input.nextInt();
        System.out.println("Input Lebar (cm)");
        lebar = input.nextInt();

        int kelilingpersegiP = (2 * panjang) + (2 * lebar);
        System.out.println("Hasil Keliling Persegi Panjang adalah = " + kelilingpersegiP);
    }
}
