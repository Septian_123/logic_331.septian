package BangunDatar;

import java.util.Scanner;

public class Lingkaran {
    private static Scanner input = new Scanner(System.in);

    private static int jari_jari;

    public static void Luas () {
        // Lingkaran
        System.out.println("Perhitungan Luas Lingkaran : ");
        System.out.println("Input Jari-Jari (cm)");
        jari_jari = input.nextInt();

        double luaslingkar = 3.14 * (jari_jari * jari_jari);

        System.out.println("Hasil Luas Lingkaran adalah = " + luaslingkar);
    }
    public static void Keliling ()
    {
        System.out.println("Perhitungan Keliling Lingkaran : ");
        System.out.println("Input Jari-Jari (cm)");
        jari_jari = input.nextInt();

        double kelilinglingkar = 2 * (22/7) * jari_jari;
        System.out.println("Hasil Keliling Lingkaran adalah = " + kelilinglingkar);

}
}
