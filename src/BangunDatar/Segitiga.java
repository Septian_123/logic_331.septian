package BangunDatar;

import java.util.Scanner;

public class Segitiga {
    private static Scanner input = new Scanner(System.in);

    private static int alas;
    private static int tinggi;
    private static int miring;

    public static void Luas() {
        System.out.println("Perhitungan Luas Segitiga : ");
        System.out.println("Input Alas (cm)");
        alas = input.nextInt();
        System.out.println("Input Tinggi (cm)");
        tinggi = input.nextInt();


        double luas = 0.5 * alas * tinggi;
        System.out.println("Hasil Luas Segitiga adalah = " + luas);
    }

    public static void Keliling()
    {
        System.out.println("Perhitungan Keliling Segitiga");
        System.out.println("Input Alas Segitiga : ");
        alas = input.nextInt();
        System.out.println("Input Sisi Miring (cm)");
        miring = input.nextInt();
        double kelilingsegitiga =  alas + (miring * 2);

        System.out.println("Hasil Keliling Segitiga adalah = " + kelilingsegitiga);
    }
}
