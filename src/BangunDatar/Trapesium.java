package BangunDatar;

import java.util.Scanner;

public class Trapesium {
    private static Scanner input = new Scanner(System.in);

    private static int sisiatastrape;
    private static int sisibawahtrape;
    private static int sisikiritrape;
    private static int sisikanantrape;
    private static int tinggitrape;

    public static void Luas () {
        // Luas Trapesium
        System.out.println("Perhitungan Luas Trapesium : ");
        System.out.println("Input Sisi Atas (cm)");
        sisiatastrape = input.nextInt();
        System.out.println("Input Sisi Bawah (cm)");
        sisibawahtrape = input.nextInt();
        System.out.println("Input Tinggi (cm)");
        tinggitrape = input.nextInt();

        double luastrape = 0.5 * (sisiatastrape + sisibawahtrape) * tinggitrape;

        System.out.println("Hasil Perhitungan Luas Trapesium adalah = " + luastrape);
    }
    public static void Keliling ()
    {
//        keliling trape
        sisiatastrape = input.nextInt();
        System.out.println("Input Sisi Bawah (cm)");
        sisibawahtrape = input.nextInt();
        System.out.println("Input Sisi Kiri (cm)");
        sisikiritrape = input.nextInt();
        System.out.println("Input Sisi Kanan (cm)");
        sisikanantrape = input.nextInt();
        System.out.println("Input Tinggi (cm)");

        int kelilingtrape = sisiatastrape + sisibawahtrape + sisikiritrape + sisikanantrape;

        System.out.println("Hasil Perhitungan Keliling Trapesium adalah = " + kelilingtrape);

    }
}
