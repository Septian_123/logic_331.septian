package BangunDatar;

import java.util.Scanner;

public class Persegi {
    private static Scanner input = new Scanner(System.in);

    private static int sisi;

    public static void Luas ()
    {
        System.out.println("Perhitungan Luas Persegi : ");
        System.out.println("Input Sisi (cm)");
        sisi = input.nextInt();

        double luaspersegi = sisi * sisi;
        System.out.println("Hasil Luas Persegi adalah = " + luaspersegi);
    }
    public static void Keliling ()
    {
        System.out.println("Perhitungan Keliling Persegi : ");
        System.out.println("Input Sisi (cm)");
        sisi = input.nextInt();

        double kelilingpersegi = 4 * sisi;
        System.out.println("Hasil Keliling Persegi adalah = " + kelilingpersegi);
    }
}
