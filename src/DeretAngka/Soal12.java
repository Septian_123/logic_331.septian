package DeretAngka;

public class Soal12 {
    public static void Resolve(int n){
        int hitung = 0;
        int jumlahData = 0;
        int counter = 0;

        int [] hasil = new int[n];

        for (int i = 0; i < n * 10; i++){
            for (int j = 1; j <= i; j++){
                if (i % j == 0)
                {
                    hitung++;
                }
            }
            if (hitung == 2){
                hasil[counter] = i;
                counter++;
                jumlahData++;
            }
            if (jumlahData == n)
            {
                break;
            }
            hitung = 0;
        }
        Uttility.PrintArray1D(hasil);
    }
}

