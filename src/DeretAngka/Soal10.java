package DeretAngka;

import org.w3c.dom.ls.LSOutput;

public class Soal10 {public static void Resolve(int n) {

    int resolve = 3;
    int count = 1;
    int[] results = new int[n];

    for (int i = 0; i < n; i++) {
        if (resolve % 4 == 0) {
            results[i] = 0;
            resolve += 3;
        } else {
            results[i] = resolve;
            resolve *= 3;
        }
    }
    Uttility.PrintArray10(results);
}
}

