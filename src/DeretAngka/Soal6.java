package DeretAngka;

public class Soal6 {
    public static void Resolve(int n) {

        int resolve = 1;
        int count = 1;
        int[] results = new int[n];

        for (int i = 0; i < n; i++) {
            if (resolve % 3 == 0) {
                results[i] = 0;
                resolve += 4;

            } else {
                results[i] = resolve;
                resolve += 4;

            }
        }

        Uttility.PrintArray1D(results);
    }
}
