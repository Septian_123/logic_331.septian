package DeretAngka;

public class Soal3
{
    public static void Resolve (int n) {
        int kaliempat = 1;
        int[] results = new int[n];

        for (int i = 0; i < n; i++) {
            results[i] = kaliempat;
            kaliempat += 4;
        }
        Uttility.PrintArray1D(results);
    }
}
