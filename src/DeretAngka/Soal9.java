package DeretAngka;

public class Soal9 {
    public static void Resolve(int n) {

        int resolve = 4;
        int count = 1;
        int[] results = new int[n];

        for (int i = 0; i < n; i++) {
            if (count % 3 == 0) {
                results[i] = 0;
                count += 1;
            } else {
                results[i] = resolve;
                resolve *= 4;
                count += 1;
            }
        }
        Uttility.PrintArray1D(results);
    }
}
