package DeretAngka;

public class Soal8 {
    public static void Resolve(int n) {

        int resolve = 3;
        int[] results = new int[n];

        for (int i = 0; i < n; i++) {
            results[i] = resolve;
            resolve *= 3;
        }
        Uttility.PrintArray1D(results);
    }
}
