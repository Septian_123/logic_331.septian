package DeretAngka;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);


        int pilihan = 0;
        int n = 0;
        boolean flag = true;
        String answer;

        while (flag) {
            System.out.println("Pilih Soal dari 1 - 12 : ");
            pilihan = input.nextInt();

            while (pilihan < 1 || pilihan > 12) {
                System.out.println("Nomor Soal tidak Tersedia");
                pilihan = input.nextInt();
            }

            System.out.println("Masukkan Nilai n : ");
            n = input.nextInt();

            switch (pilihan) {
                case 1:
                    Soal1.Resolve(n);
                    break;
                case 2:
                    Soal2.Resolve(n);
                    break;
                case 3:
                    Soal3.Resolve(n);
                    break;
                case 4:
                    Soal4.Resolve(n);
                    break;
                case 5:
                    Soal5.Resolve(n);
                    break;
                case 6:
                    Soal6.Resolve(n);
                    break;
                case 7:
                    Soal7.Resolve(n);
                    break;
                case 8:
                    Soal8.Resolve(n);
                    break;
                case 9:
                    Soal9.Resolve(n);
                    break;
                case 10:
                    Soal10.Resolve(n);
                    break;
                case 11:
                    Soal11.Resolve(n);
                    break;
                case 12:
                    Soal12.Resolve(n);
                    break;

                default:

            }
            System.out.println("Try Againt? y/n");
            input.nextLine();
            answer = input.nextLine();

            if (!answer.toLowerCase().equals("y")) {
                flag = false;
            }

        }
    }
}