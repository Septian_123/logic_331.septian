package DeretAngka;

public class Soal2 {
    public static void Resolve (int n){
        int genap = 2;
        int [] results = new int [n];

        for (int i = 0; i < n; i++) {
            results[i] = genap;
            genap += 2;
        }
        Uttility.PrintArray1D(results);
    }
}
