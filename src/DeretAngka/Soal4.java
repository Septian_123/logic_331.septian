package DeretAngka;

public class Soal4 {
    public static void Resolve(int n) {

        int resolve = 2;
        int[] results = new int[n];

        for (int i = 0; i < n; i++) {
            results[i] = resolve;
            resolve += 4;
        }
        Uttility.PrintArray1D(results);
    }
}