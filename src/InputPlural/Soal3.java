package InputPlural;

import Array2D.Uttility;

import java.util.Scanner;

public class Soal3 {
    public static void Resolve () {
        Scanner input = new Scanner(System.in);

            System.out.println("Input deret angka");
            String text = input.nextLine();
            double jumlah = 0;

            int[] intArray = Utility.ConvertStringToArrayInt(text);
            int length = intArray.length;

            for (int i = 0; i < length; i++) {
                if (intArray.length % 2 == 0) {
                    int n1 = intArray[i / 2];
                    int n2 = intArray[(i / 2) + 1];
                    double n1konversi = Double.valueOf(n1);
                    double n2konversi = Double.valueOf(n2);
                    jumlah = (n1konversi + n2konversi) / 2.0;
                } else {
                    int n3 = intArray[(i - 1) / 2] + intArray[i / 2] / 2;
                    jumlah = n3;
                }
            }
            System.out.println("Hasil = " + jumlah);
        }
    }