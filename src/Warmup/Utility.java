package Warmup;

public class Utility {
    public static int[] ConvertStringToArrayInt(String text) {
        String[] textArray = text.split (" ");
        int[] intArray = new int[textArray.length];

        for (int i = 0; i < textArray.length; i++) {
            intArray[i] = Integer.parseInt(textArray[i]);
        }
        return intArray;
    }
    public static void PrintArray2DD(int[][] results) {
        int count = 0;
        for (int i = 0; i < results.length; i++) {
            for (int j = 0; j < results[0].length; j++) {
                if (i == 0) {
                    System.out.print(results[i][j] + " ");
                }
                else if (i == 1) {
                    if (count == 2) {
                        System.out.print("-" + results[i][j] + " ");
                        count -= 2;
                    } else {
                        System.out.print(results[i][j] + " ");
                        count++;
                    }
                }
            }
            System.out.println();
        }
    }
}
