package Warmup;


import java.util.Scanner;

public class Soal5 {
    public static void Resolve (){
        Scanner input = new Scanner(System.in);

        System.out.println("Silahkan input data angka = ");
        String nilai  = input.nextLine();
        String[] stNilai = nilai.split(" ");
        int [] innilai = new int [stNilai.length];

        for (int i = 0; i < stNilai.length; i++) {
            innilai[i] = Integer.parseInt(stNilai[i]);
        }
        int panjang = stNilai.length;
        double positif = 0;
        double negative = 0;
        double kosong = 0;

        for (int i = 0; i < panjang; i++) {
            if (innilai[i] < 0){
                negative++;
            }
            if (innilai[i] == 0){
                kosong++;
            }
            if (innilai[i] > 0){
                positif++;
            }
        }
        negative = (negative/panjang);
        positif = (positif/panjang);
        kosong = (kosong/panjang);

        System.out.println("Hasil nilai positif = " + positif);
        System.out.println("Hasil nilai negatif = " + negative);
        System.out.println("Hasil nilai zero = " + kosong);
    }
}
