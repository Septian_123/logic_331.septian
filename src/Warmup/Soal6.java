package Warmup;

import java.util.Scanner;

public class Soal6 {
    public static void Resolve () {
        Scanner input = new Scanner(System.in);
        System.out.println("Silahkan masukkan input");
        int n = input.nextInt();

        int baris = n;

        for (int i = 1; i <= baris; i++) {
            for (int j = 1; j <= baris - i; j++) {
                System.out.print("  ");
            }
            for (int k = 1; k <= i ; k++) {
                System.out.print("* ");
            }
            System.out.println();
        }
    }
}