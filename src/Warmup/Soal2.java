package Warmup;

import java.util.Scanner;

public class Soal2 {

    public static void Resolve () {
        Scanner input = new Scanner(System.in);
        int jam = 0;
        int menit = 0;
        int detik = 0;
        int pilihan;

        System.out.println("Masukkan input waktu = ");
        System.out.println("Input Jam = ");
        jam = input.nextInt();
        System.out.println("Input Menit = ");
        menit = input.nextInt();
        System.out.println("Input Detik = ");
        detik = input.nextInt();
        System.out.println("Silahkan masukkan tipe waktu AM/PM");
        System.out.println("1. AM       2. PM");
        pilihan = input.nextInt();

        switch (pilihan){
            case 1 :
                System.out.println("Output jam : " + jam + ":" + menit + ":" + detik);
                break;
            case 2 :
                System.out.println("Output jam : " + ((jam)+ 12) + ":" + menit + ":" + detik);
                break;
            default:
        }

    }

}
