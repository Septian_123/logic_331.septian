package Warmup;

import java.util.Scanner;

public class Soal10 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("masukkan input angka = ");
        System.out.println("baris A = ");
        String barisA = input.nextLine();
        System.out.println("baris B = ");
        String barisB = input.nextLine();

        int[] nilaiA = Utility.ConvertStringToArrayInt(barisA);
        int[] nilaiB = Utility.ConvertStringToArrayInt(barisB);
        int panjang = nilaiA.length;
        int pointA = 0;
        int pointB = 0;

        for (int i = 0; i < panjang; i++) {
            if (nilaiA[i] > nilaiB[i]){
                pointA ++;
            } else if (nilaiA[i] == nilaiB[i]) {
            } else {
                pointB ++;
            }
        }
        System.out.print("Output = " + pointA + ", "+ pointB );
    }
}