package BankBBF;

import java.util.Scanner;

public class CreatePIN {
    private static Scanner input = new Scanner(System.in);
    public static String pin;
    private static String masukanpin;

    public static String buatpin() {

        boolean flag = true;
        while (flag) {
            System.out.println("Silahkan Buat PIN Baru");
            System.out.println("Buatlah menggunakan 6 digit angka");
            System.out.println("Masukkan angka : ");

            pin = input.nextLine();

            if (pin.length() != 6) {
                System.out.println("Gagal membuat PIN, Silahkan ulangi kembali");
                pin = input.nextLine();
            }
            if (!Uttility.IsNumeric(pin)) {
                System.out.println("PIN harus berupa Angka");
            }
            flag = false;
        }
        return pin;

    }

    public static boolean validPIN(String masukanpin) {

        int count = 1;
        boolean flag =true;

        System.out.println("Silahkan masukkan PIN Anda : ");
        masukanpin = input.nextLine();

        while (!masukanpin.equals(pin) && count < 3) {
            System.out.println("Maaf PIN yang Anda masukkan salah");
            masukanpin = input.nextLine();
            count++;
        }
        if (count == 3) {
            System.out.println("ATM Anda diBlokir");

            flag = false;

        }
    return true;

    }

}

