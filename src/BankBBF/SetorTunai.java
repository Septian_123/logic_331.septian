package BankBBF;

import java.util.Scanner;

public class SetorTunai {
    private static Scanner input = new Scanner(System.in);
    public static int saldo = 0;
    private static String nilai;
    private static  String transfer;

    public static void setor() {

        boolean flag = true;
        while (flag) {
            System.out.println("Masukkan Nominal yang diinginkan : ");

            String tunai = input.nextLine();

            if (Uttility.IsNumeric(tunai)) {
                int angkatunai = Integer.parseInt(tunai);
                if (angkatunai >= 2500000) {
                    System.out.println("ATM ini tidak bisa melakukan penyetoran dana lebih dari Rp. 25.000.000,00.");
                } else {
                    System.out.println("Apakah anda yakin untuk melanjutkan transaksi?");
                    System.out.println("y/n");
                    String answer;
                    answer = input.nextLine();

                    if (answer.toLowerCase().equals("y")) {
                        System.out.println("Transaksi anda berhasill");
                        saldo = angkatunai;
                        System.out.println("Saldo anda =" + saldo);

                    }
                    System.out.println("Apakah anda ingin melakukan transaksi kembali?");
                    System.out.println("y/n");
                    String answer2;
                    answer2 = input.nextLine();
                    if (answer2.toLowerCase().equals("y")) {
                        flag = true;
                    } else {
                        Pilihan.pilih();
                    }
                }
            }

        }

    }

    public static void antarrek() {
        boolean flag = false;
        while (flag) {
            System.out.println("Silahkan Memasukkan Nominal Transfer Sesuai dengan yang Anda inginkan = ");
            nilai = input.nextLine();
            Transfer.setor();

            if (nilai.length() != 7) {
                System.out.println("Masukan nominal Transfer sejumlah 7 digit = ");
            } else {
                System.out.println("Apakah anda yakin untuk melanjutkan transaksi?");
                System.out.println("y/n");
                String answer;
                answer = input.nextLine();
                if (answer.toLowerCase().equals("y")) {
                    int angkanilai = Integer.parseInt(nilai);
                    if (saldo > angkanilai) {
                        System.out.println("maaf saldo anda tidak mencukupi silahkan lakukan transaksi ulang");
                        flag = false;
                    }
                    System.out.println("Transfer berhasil");

                }
            }
            Uttility.IsNumeric(nilai);
        }
    }
}
