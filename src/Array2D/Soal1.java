package Array2D;

import java.util.Scanner;

public class Soal1 {
    public static void Resolve () {
        Scanner input = new Scanner(System.in);

        System.out.println("Masukkan input nilai n = ");
        int n = input.nextInt();

        int [][] results = new int[2][n];
        int angka = 0;
        int angka2 = 1;

        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < n; j++) {
                if (i==0){
                    results [i][j] = angka;
                    angka++;
                }
                else if (i==1){
                    results[i][j] = angka2;
                    angka2*=3;
                }
            }
        }
        Uttility.PrintArray2D(results);
    }
}
