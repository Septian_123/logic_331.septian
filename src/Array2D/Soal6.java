package Array2D;

import java.util.Scanner;

public class Soal6 {

    public static void Resolve () {
        Scanner input = new Scanner(System.in);

        System.out.println("Input n");
        int n = input.nextInt();

        int [][] results = new int[3][n];
        int angka = 0;
        int angka2 = 1;
        int nilaibawah = 1;
        int nilaiatas = 0;

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < n; j++) {
                if (i==0){
                results[i][j] = angka;
                angka++;
                }
                else if (i==1){
                    results[i][j] = angka2;
                    angka2 *= n;
                } else if (i == 2) {
                    results[i][j] = nilaiatas + nilaibawah;
                    nilaiatas++;
                    nilaibawah *=n;

                }
            }
        }
        Uttility.PrintArray2D(results);
    }
}
