package Array2D;

import java.util.Scanner;

public class Soal8 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        int angka = 0;
        int genap = 0;
        int ganjil = 0;

        System.out.println("masukkan nilai n = ");
        int n = input.nextInt();
        int[][] results = new int[3][n];

        for (int i = 0; i < 3; i++){
            for (int j = 0; j < n; j++){
                if (i == 0)
                {
                    results[i][j] = angka;
                    angka++;
                }
                else if (i == 1)
                {
                    results[i][j] = genap;
                    genap += 2;
                }
                else if (i == 2)
                {
                    results[i][j] = ganjil;
                    ganjil += 3;
                }
            }
        }
        Uttility.PrintArray2D(results);
    }
}

