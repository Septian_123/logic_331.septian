package Array2D;

import java.util.Scanner;

public class Soal10 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        int nilaiA = 0;
        int nilaiB = 0;
        int nilaiC = 0;

        System.out.println("Masukkan nilai n = ");
        int n = input.nextInt();
        int[][] results = new int[3][n];

        for (int i = 0; i < 3; i++){
            for (int j = 0; j < n; j++){
                if (i == 0)
                {
                    results[i][j] = nilaiA;
                    nilaiA ++;
                }
                else if (i == 1)
                {
                    results[i][j] = nilaiB;
                    nilaiB += 3;
                }
                else if (i == 2)
                {
                    results[i][j] = nilaiC;
                    nilaiC += 4;
                }
            }
        }
        Uttility.PrintArray2D(results);
    }
}

