package Array2D;

public class Uttility {
    public static void PrintArray2D(int[][] results) {
        for (int i = 0; i < results.length; i++) {
            for (int j = 0; j < results[0].length; j++) {
                System.out.print(results[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static void PrintArray2DD(int[][] results) {
        int count = 0;
        for (int i = 0; i < results.length; i++) {
            for (int j = 0; j < results[0].length; j++) {
                if (i == 0) {
                    System.out.print(results[i][j] + " ");
                }
                else if (i == 1) {
                    if (count == 2) {
                        System.out.print("-" + results[i][j] + " ");
                        count -= 2;
                    } else {
                        System.out.print(results[i][j] + " ");
                        count++;
                    }
                }
            }
            System.out.println();
        }
    }
}
