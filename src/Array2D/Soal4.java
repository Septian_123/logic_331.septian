package Array2D;

import java.util.Scanner;

public class Soal4 {
    public static void Resolve () {
        Scanner input = new Scanner(System.in);

        System.out.println("Input n");
        int n = input.nextInt();

        int [][] results = new int[2][n];
        int angka = 0;
        int angka2 = 1;
        int tengah = 5;

        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < n; j++) {
                if (i == 0){
                    results[i][j] = angka;
                    angka++;
                }
                else if (i == 1) {
                    if (j % 2 == 0) {
                        results[i][j] = angka2;
                        angka2++;
                    } else {
                        results[i][j] = tengah;
                        tengah += 5;
                    }
                }
            }
        }
        Uttility.PrintArray2D(results);
    }
}

