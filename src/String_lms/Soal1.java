package String_lms;

import java.util.Scanner;

 public class Soal1 {
    private static Scanner input = new Scanner(System.in);

    public static void Resolve (){
        Scanner input = new Scanner(System.in);

        System.out.println("Soal tentang Camel Case");
        System.out.println("Contoh inputan = saveChangesInTheEditor");
        System.out.println("Jawab = ");
        String answer = input.nextLine();
        int count = 1;

        char[] kata = answer.toCharArray();

        for (int i = 0; i < kata.length; i++) {
            if (Character.isUpperCase(kata[i])){
                count++;
            }
        }
        System.out.println("Jumlah kata = " + count);
    }

}
