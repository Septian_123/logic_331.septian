package String_lms;

import java.util.Scanner;

public class Soal3 {

    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.print("Input: ");
        String text = input.nextLine();

        char[] charArray = text.toCharArray();

        System.out.print("Output: ");
        for (int i = 0; i < charArray.length; i++) {
            charArray[i] += 3;
            System.out.print(charArray[i]);
        }
        System.out.println();
    }
}
