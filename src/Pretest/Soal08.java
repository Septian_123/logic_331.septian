package Pretest;

import java.util.Scanner;

public class Soal08 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        System.out.println("Masukkan input deret angka = ");
        int deret = input.nextInt();
        int[] results = new int[deret];
        int counter = 0;
        int hitung = 0;
        int jumlah = 0;
        int helper = 0;
        int helper1 = 1;

        int [] hasilFibo = new int[deret];
        int[] hasilPrima = new int[deret];

        // Proses Bilangan Prima //
        for (int i = 0; i < deret*10; i++) {
            for (int j = 1; j <= i ; j++) {
                if (i % j == 0)
                {
                    hitung++;
                }
            }
            if (hitung == 2) {
                hasilPrima[counter] = i;
                counter++;
                jumlah++;
            }
            if (jumlah == deret) {
                break;
            }
            hitung = 0;
        }
        System.out.print("Bilangan Prima = ");
        for (int i = 0; i < deret; i++) {
            System.out.print(hasilPrima[i] + ", ");
        }
        System.out.println();
        //deret Fibonacci//
        for (int i = 0; i < deret; i++) {
            if(i == 0)
            {
                hasilFibo[i] = helper1;
            }
            else
            {
                hasilFibo[i] = helper + helper1;
                helper = helper1;
                helper1 = hasilFibo[i];
            }
        }

        System.out.print("Bilangan Fibonacci = " );
        for (int i = 0; i < deret; i++) {
            System.out.print(hasilFibo[i] + ", ");
        }
        System.out.println();

        System.out.print("Output = " );
        for (int i = 0; i < deret; i++) {
            results[i] = hasilFibo[i] + hasilPrima[i];
            System.out.print(results[i] + ", ");
        }

    }
}