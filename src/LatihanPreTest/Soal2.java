package LatihanPreTest;

import java.util.Scanner;

public class Soal2 {
    public static void Resolve () {
        Scanner input = new Scanner(System.in);

        System.out.println("Periksa kata/angka apakah termasuk Palindrum atau tidak");
        System.out.println("Masukkan input kata/angka = ");
        String n = input.nextLine().toLowerCase();

        StringBuilder inputn = new StringBuilder();
        inputn.append(n);
        inputn.reverse();
        String inputstring = inputn.toString();

        if (inputstring.equals(n)){
            System.out.println("YES");
        }
        else {
            System.out.println("NO");
        }
    }
}
