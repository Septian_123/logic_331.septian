package LatihanPreTest;

import java.text.DecimalFormat;
import java.util.Scanner;

public class Soal10 {
    public static void Resolve () {
        Scanner input = new Scanner(System.in);

        System.out.println("Masukkan cupcake yang akan dibuat = ");
        int n = input.nextInt();
        double gula = (double) 100 / 15;
        double terigu = (double) 125/ 15;
        double susu = (double) 100 / 15;

        double ngula = gula * n;
        double nterigu = terigu * n;
        double nsusu = susu * n;

        DecimalFormat deci = new DecimalFormat();
        deci.applyPattern("#####.##");

        System.out.println("bahan yang harus di siapkan  jika ingin membuat = " + n + "cake" );
        System.out.println("terigu = " + deci.format(nterigu) + "gr");
        System.out.println("gula = " + deci.format(ngula) + "gr");
        System.out.println("susu = " + deci.format(nsusu) + "mL");
    }
}
