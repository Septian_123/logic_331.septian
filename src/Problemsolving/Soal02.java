package Problemsolving;

import java.util.Scanner;

public class Soal02 {
    public static void Resolve (){

            Scanner input = new Scanner(System.in);

            System.out.println("Masukkan input toko yang dituju");
            System.out.println("Contoh 1-2-3-4");
            System.out.println("Toko 1 = 0.5 km");
            System.out.println("Toko 2 = 2 km");
            System.out.println("Toko 3 = 3.5 km");
            System.out.println("Toko 4 = 5 km");
            String masukkan = input.nextLine();
            String[] tokoArray = masukkan.split("-");

            int [] posisiinput = new int[tokoArray.length];

            for (int i = 0; i < tokoArray.length; i++) {
                    posisiinput[i] = Integer.parseInt(tokoArray[i]);
            }
            double[] jaraktoko = {0, 0.5, 2, 3.5, 5};
            //jarak yang total input
            double npindah = 0;
            // jarak antar toko
            double jarak = 0;
            // jarak pada soal (0,5-1,5-2-3,5)
            double jaraksebelumnya = 0;
            // buat ambil posisi data input
            int pindah = 0;

            for (int i = 0; i < posisiinput.length; i++) {
                    pindah = posisiinput[i];
                    jarak = jaraktoko[pindah] - jaraksebelumnya;
                    jaraksebelumnya = jaraktoko[pindah];
                    npindah += Math.abs(jarak);
            }
            double output = jaraktoko[pindah] + npindah;
            double kecepatan = 0.5; //kecepatan 0.5 permenit
            double delaytoko = 10 * posisiinput.length;
            double waktu = (output/kecepatan) + delaytoko;

            System.out.println("nilai = " + waktu + "menit");
    }
}
