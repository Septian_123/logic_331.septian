package Problemsolving;

import java.util.Scanner;



public class Soal10 {
    private static String[] datawadah = {"teko", "botol", "gelas", "cangkir"};
    public static void Resolve() {
        Scanner input = new Scanner (System.in);

        System.out.println("Masukkan wadah minuman yang akan di input");
        System.out.println("Wadah minuman yang di input : ");
        System.out.println("1. Botol");
        System.out.println("2. Teko");
        System.out.println("3. Gelas");
        System.out.println("4. Cangkir");
        int pilihan = input.nextInt();
        pilihan -= 1;

        System.out.println("Masukkan Jumlah input = ");
        int jumlah = input.nextInt();

        System.out.println("Masukkan input yang ingin dibandingkan");
        System.out.println("1. Botol");
        System.out.println("2. Teko");
        System.out.println("3. Gelas");
        System.out.println("4. Cangkir");
        int pilihan2 = input.nextInt();

        switch (pilihan2) {
            case 1:
                KonversiTeko(datawadah[pilihan], jumlah);
                break;
            case 2:
                KonversiBotol(datawadah[pilihan], jumlah);
                break;
            case 3:
                KonversiGelas(datawadah[pilihan], jumlah);
                break;
            case 4:
                KonversiCangkir(datawadah[pilihan], jumlah);
                break;
            default:
                break;
        }

    }

    // teko ==> botol ==> gelas ==> cangkir
    //   1  ==>   5   ==>  10   ==>   25
    //       *5         *2         *2,5

    private static void KonversiTeko(String wadah, int jumlah) {
        double hasilKonversi = 0;
        String namaKonversi = datawadah[0];
        if (wadah.equals(datawadah[1])){
            hasilKonversi = (double) jumlah / 5;
        } else if (wadah.equals(datawadah[2])) {
            hasilKonversi = (double) (jumlah / 2) / 5 ;
        }else if (wadah.equals(datawadah[3])){
            hasilKonversi = ((jumlah / 2.5) / 2) / 5;
        }else {
            hasilKonversi = jumlah;
        }
        System.out.println(jumlah + " " + wadah + " = " + hasilKonversi + " " + namaKonversi);
    }

    private static void KonversiBotol(String wadah, int jumlah) {
        double hasilKonversi = 0;
        String namaKonversi = datawadah[1];
        if (wadah.equals(datawadah[0])){
            hasilKonversi = (double) jumlah * 5;
        } else if (wadah.equals(datawadah[2])) {
            hasilKonversi = (double) jumlah / 2;
        }else if (wadah.equals(datawadah[3])){
            hasilKonversi = (jumlah / 2.5) / 2;
        }else {
            hasilKonversi = jumlah;
        }
        System.out.println(jumlah + " " + wadah + " = " + hasilKonversi + " " + namaKonversi);
    }

    private static void KonversiGelas(String wadah, int jumlah) {
        double hasilKonversi = 0;
        String namaKonversi = datawadah[2];
        if (wadah.equals(datawadah[0])){
            hasilKonversi = (double) (jumlah * 5) * 2;
        } else if (wadah.equals(datawadah[1])) {
            hasilKonversi = (double) jumlah * 2;
        }else if (wadah.equals(datawadah[3])){
            hasilKonversi = (jumlah / 2.5);
        }else {
            hasilKonversi = jumlah;
        }
        System.out.println(jumlah + " " + wadah + " = " + hasilKonversi + " " + namaKonversi);
    }

    private static void KonversiCangkir(String wadah, int jumlah) {
        double hasilKonversi = 0;
        String namaKonversi = datawadah[3];
        if (wadah.equals(datawadah[0])){
            hasilKonversi = ((jumlah * 5) * 2) * 2.5;
        } else if (wadah.equals(datawadah[1])) {
            hasilKonversi = (jumlah * 2) * 2.5;
        }else if (wadah.equals(datawadah[3])){
            hasilKonversi = jumlah * 2.5;
        }else {
            hasilKonversi = jumlah;
        }
        System.out.println(jumlah + " " + wadah + " = " + hasilKonversi + " " + namaKonversi);
}

}


