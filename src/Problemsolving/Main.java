package Problemsolving;

import Array2D.*;

import java.util.Scanner;

public class Main {
        public static void main(String[] args) {
            Scanner input = new Scanner(System.in);


            int pilihan = 0;
            int n = 0;
            boolean flag = true;
            String answer;

            while (flag) {
                System.out.println("Pilih Soal dari 1 - 12 : ");
                pilihan = input.nextInt();

                while (pilihan < 1 || pilihan > 12) {
                    System.out.println("Nomor Soal tidak Tersedia");
                    pilihan = input.nextInt();
                }

                switch (pilihan) {
                    case 1:
                        Soal01.Resolve();
                        break;
                    case 2:
                        Soal02.Resolve();
                        break;
                    case 3:
                        Soal03.Resolve();
                        break;
                    case 4:
                        Soal04.Resolve();
                        break;
                    case 5:
//                        Soal4.Resolve();
                        break;
                    case 6:
//                        Soal5.Resolve();
                        break;
                    case 7:
//                        Soal6.Resolve();
                        break;
                    case 8:
//                        Soal7.Resolve();
                        break;
                    case 9:
//                        Soal8.Resolve();
                        break;
                    case 10:
//                        Soal9.Resolve();
                        break;
                    case 11:
                        Soal11.Resolve();
                        break;
                    case 12:
//                        Soal11.Resolve();
                        break;
                    case 13:
//                Soal12.Resolve();
                        break;
                    default:
                }
                System.out.println("Try Againt? y/n");
                input.nextLine();
                answer = input.nextLine();

                if (!answer.toLowerCase().equals("y")) {
                    flag = false;
                }

            }
        }
    }

