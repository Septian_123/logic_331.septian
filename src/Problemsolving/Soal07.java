package Problemsolving;

import java.util.Scanner;

public class Soal07 {
    public static void Resolve (){
        Scanner input = new Scanner(System.in);

        System.out.println("Masukkan pola lintasan = ");
        String pola = input.nextLine();
        System.out.println("Masukkan cara jalan = ");
        String jalan = input.nextLine();

        char [] polachar = pola.toCharArray();
        char [] jalanchar = jalan.toCharArray();

        int lompat = 0;
        int energi = 0;
        String output = "";

        for (int i = 0; i < jalanchar.length; i++) {
            char alop = polachar [i+lompat];
            char gerak = polachar [i];
            if (gerak== 'w' && alop == '-'){
                lompat += 0;
                energi -= 1;
                output = String.valueOf(energi);
            }
            else if (gerak == 'w' &&  alop == 'o'){
                output = "Capek capeek capek";
                break;
            } else if (gerak == 'j' && (alop == '-' || alop == '0') && energi ==2) {
                lompat += 1;
                energi -= 2;
                output = String.valueOf(energi);
            } else if (gerak == 'j' && alop == 'o' && energi < 2) {
                output = "Jim capek pengen beli trek";
                break;
            }
        }
        System.out.println("Output = " + output);
    }
}
